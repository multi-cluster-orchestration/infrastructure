# Infrastructure (Kubernetes Orchestration Platform)

This repository contains files for the provisioning of Kubernetes clusters as well as the installation and configuration of ArgoCD. There is also a set of Gitlab CI/CD files that enable the automation of the creation and destruction of these resources.

A management cluster with ArgoCD must be created for all worker clusters to connect to. This repository automated all of it.

This repository has an article written about it. [Check it out here.](https://medium.com/@sami_alakus/orchestrating-the-orchestrator-kubernetes-management-in-multi-clustered-environments-e0c91ce62490)

## Getting Started

We will create two clusters:
- do-management - A Digital Ocean cluster we'll use as our management instance and install ArgoCD to
- do-production - Another Digital Ocean cluster that will run our production workloads

## Prerequisits

**Digital Ocean API Key**

You'll need to create a Digital Ocean API key and store is as a Gitlab CI/CD variable. [Follow their guide for more infomration.](https://docs.digitalocean.com/reference/api/create-personal-access-token/)

### Management Cluster

1. Review the `clusters/do-management` directory and change for your needs
2. Ensure the `do_api_key` has been added as Gitlab CI/CD variables
    - The variable is passed through to Terraform in our `clusters/do-management/.gitlab-ci.yml` file as `TF_VAR_do_api_key`
3. Commit and push these changes to create the management cluster and install ArgoCD to it
4. Collect the ArgoCD auth token with `terraform output automation_user_token` and create the `argocd_auth_token` Gitlab CI/CD variable with the output as the value

With this completed you will need to ensure ArgoCD has a valid URL and you can connect to it with SSL. I utilise the platform to deploy an ArgoCD ingress through the [management-applications](https://gitlab.com/multi-cluster-orchestration/management-applications) repository.

### Worker Cluster

1. Review the `clusters/do-production` directory and change for your needs
2. Ensure the `do_api_key` has been added as Gitlab CI/CD variables
    - The variable is passed through to Terraform in our `clusters/do-production/.gitlab-ci.yml` file as `TF_VAR_do_api_key`
3. Commit and push these changes to create the cluster resources and connect it to the management cluster


### Creating Additional Clusters

You're able to create as many clusters as you like in this system. To do so create a new directory under `clusters` with your Terraform files and create a `.gitlab-ci.yml` file there too with the following contents:

```
variables:
  TF_ROOT: ${CI_PROJECT_DIR}/clusters/${CLUSTER_NAME}
  TF_STATE_NAME: clusters/${CLUSTER_TYPE}-${CLUSTER_NAME}
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CLUSTER_TYPE}-${CLUSTER_NAME}
  TF_VAR_argocd_auth_token: ${ARGOCD_ACCESS_TOKEN}
  TF_AUTO_DEPLOY: "true"           # Can set to "false" to enforce manual deployment triggers
  TF_VAR_do_api_key: ${DO_API_KEY} # Set Terraform variables with the TF_VARS prefix. Should be changed to suit whatever cloud provider you choose

stages:
  - initialise
  - build
  - deploy
  - destroy

include:
  - local: ${CI_PROJECT_DIR}/.gitlab-templates/terraform-base.gitlab-ci.yml # Links to the Gitlab CI/CD pipeline each cluster shares
```
