output "kubeconfig" {
  value = digitalocean_kubernetes_cluster.cluster.kube_config.0.raw_config
  sensitive = true
}

output "cluster_endpoint" {
  value = local.kubeconfig_object.clusters[0].cluster.server
  sensitive = true
}

output "ca_data" {
  value = base64decode(local.kubeconfig_object.clusters[0].cluster.certificate-authority-data)
  sensitive = true
}

output "bearer_token" {
  value = local.kubeconfig_object.users[0].user.token
  sensitive = true
}