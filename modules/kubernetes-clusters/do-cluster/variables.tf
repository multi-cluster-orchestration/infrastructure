variable "api_key" {
    description = "Digital Ocean API key"
    type        = string  
}

variable "region" {
    description = "Region to deploy cluster to"
    type        = string
    default     = "syd1"
}

variable "name" {
    description = "Cluster name"
    type        = string
}

variable "kube_version" {
    description = "Kube Versions"
    type        = string
    default     = "1.27.4-do.0"
}

variable "node_count" {
    description = "Number of worker nodes"
    type        = number
    default     = 1
}

variable "node_plan" {
    description = "Type of node to use"
    type        = string
    default     = "s-1vcpu-2gb"
}