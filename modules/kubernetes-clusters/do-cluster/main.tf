resource "digitalocean_kubernetes_cluster" "cluster" {
  name    = var.name
  region  = var.region
  version = var.kube_version

  node_pool {
    node_count = var.node_count
    size       = var.node_plan
    name       = "${var.name}-pool"
  }
}