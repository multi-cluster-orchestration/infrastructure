locals {
  kubeconfig_object = yamldecode(digitalocean_kubernetes_cluster.cluster.kube_config.0.raw_config)
}