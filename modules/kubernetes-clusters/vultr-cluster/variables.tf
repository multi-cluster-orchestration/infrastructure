variable "api_key" {
    description = "Vultr API key"
    type        = string  
}

variable "region" {
    description = "Vultr region to deploy cluster to"
    type        = string
    default     = "mel"
}

variable "name" {
    description = "Cluster name"
    type        = string
}

variable "kube_version" {
    description = "Kube Versions"
    type        = string
    default     = "v1.26.5+1"
}

variable "node_count" {
    description = "Number of worker nodes"
    type        = number
    default     = 1
}

variable "node_plan" {
    description = "Type of node to use"
    type        = string
    default     = "vc2-2c-4gb"
}