output "kubeconfig" {
  value = base64decode(vultr_kubernetes.cluster.kube_config)
  sensitive = true
}

output "cluster_endpoint" {
  value = local.kubeconfig_object.clusters[0].cluster.server
  sensitive = true
}

output "ca_data" {
  value = base64decode(local.kubeconfig_object.clusters[0].cluster.certificate-authority-data)
  sensitive = true
}

output "user_cert_data" {
  value = base64decode(local.kubeconfig_object.users[0].user.client-certificate-data)
  sensitive = true
}

output "user_cert_key" {
  value = base64decode(local.kubeconfig_object.users[0].user.client-key-data)
  sensitive = true
}