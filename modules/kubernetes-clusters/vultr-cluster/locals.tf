locals {
  kubeconfig_object = yamldecode(base64decode(vultr_kubernetes.cluster.kube_config))
}