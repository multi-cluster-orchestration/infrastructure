resource "vultr_kubernetes" "cluster" {
    region  = var.region
    label   = var.name
    version = var.kube_version

    node_pools {
        node_quantity = var.node_count
        plan          = var.node_plan
        label         = "${var.name}-pool"
    }
}