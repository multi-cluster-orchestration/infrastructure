terraform {
  required_providers {
    argocd = {
      source = "oboukili/argocd"
      version = "6.0.1"
    }

    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# ArgoCD provider 
provider "argocd" {
  server_addr = var.argocd_url
  auth_token  = var.argocd_auth_token
  grpc_web    = true
}

# DigitalOcean provider
provider "digitalocean" {
  token = var.do_api_key
}