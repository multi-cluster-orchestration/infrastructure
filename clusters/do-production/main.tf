module "do-cluster" {
    source = "../../modules/kubernetes-clusters/do-cluster"
    
    api_key      = var.do_api_key
    name         = var.name
    region       = var.region
    node_plan    = var.node_plan
    node_count   = var.node_count
    kube_version = var.kube_version
}

# Attach cluster to Argo
module "argo-cluster" {
    depends_on = [module.do-cluster]
    source = "../../modules/argocd/cluster"

    argocd_url          = var.argocd_url
    argocd_auth_token   = var.argocd_auth_token

    name                = var.name
    cluster_labels      = var.cluster_labels

    cluster_endpoint    = module.do-cluster.cluster_endpoint
    ca_data             = module.do-cluster.ca_data
    bearer_token        = module.do-cluster.bearer_token
}