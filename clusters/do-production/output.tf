output "kubeconfig" {
    value = module.do-cluster.kubeconfig
    sensitive = true
}