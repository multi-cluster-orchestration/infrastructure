# The url of our ArgoCD installation on the management cluster
argocd_url   = "argocd.management.fatherbean.com"

# Cluster name
name         = "do-production"

# Cluster labels to apply to this cluster
cluster_labels = {
  global      = "true"
  production  = "true"
}

# Digital Ocean cluster variables
region       = "syd1"
node_plan    = "s-2vcpu-4gb"
kube_version = "1.27.4-do.0"
node_count   = 2