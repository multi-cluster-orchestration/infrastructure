name         = "do-management"
region       = "syd1"
node_plan    = "s-1vcpu-2gb"
kube_version = "1.27.4-do.0"
node_count   = 2

cluster_labels = {
  global     = "true"
  management = "true"
}

# Argo repositories
repositories = [
  {
    name       = "management"
    repository = "https://gitlab.com/multi-cluster-orchestration/management-applications.git"
    label      = {
      management = "true"
    }
  },
  {
    name       = "global"
    repository = "https://gitlab.com/multi-cluster-orchestration/global-applications.git"
    label      = {
      global = "true"
    }
  },
  {
    name       = "production"
    repository = "https://gitlab.com/multi-cluster-orchestration/production-applications.git"
    label      = {
      production = "true"
    }
  }    
]