terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "= 2.5.1"
    }    

    argocd = {
      source = "oboukili/argocd"
      version = "6.0.1"
    }
  }
}

# DigitalOcean provider
provider "digitalocean" {
  token = var.do_api_key
}

# Helm provider with our cluster details
provider "helm" {
    kubernetes {
        host     = module.do-cluster.cluster_endpoint

        token                  = module.do-cluster.bearer_token
        cluster_ca_certificate = module.do-cluster.ca_data
    }
}

# ArgoCD provider for argo administration
provider "argocd" {
    username     = "admin"
    password     = local.admin_password

    port_forward_with_namespace = "argocd"

    kubernetes {
        host     = module.do-cluster.cluster_endpoint

        token                  = module.do-cluster.bearer_token
        cluster_ca_certificate = module.do-cluster.ca_data
    }
}