# General variables
variable "name" {
    description = "Cluster name"
    type        = string
}

variable "cluster_labels" {
  description = "Labels to assign to ArgoCD cluster"
  type        = map(any)
}

# Management variables
variable "repositories" {
  description = "List of maps with repository details"
  type = list(object({
    name       = string
    repository = string
    label      = map(any)
  }))
}

variable "automation_user" {
  description = "Name of automation user with token for our pipeline automation"
  type        = string
  default     = "automation"
}

variable "admin_password" {
  description = "Password to set for the admin user"
  type        = string
  default     = ""
  sensitive   = true
}

# Digital Ocean cluster variables
variable "do_api_key" {
    description = "Digital Ocean API key"
    type        = string  
}

variable "region" {
    description = "Vultr region to deploy cluster to"
    type        = string
    default     = "mel"
}

variable "kube_version" {
    description = "Kube Versions"
    type        = string
    default     = "v1.26.5+1"
}

variable "node_count" {
    description = "Number of worker nodes"
    type        = number
    default     = 1
}

variable "node_plan" {
    description = "Type of node to use"
    type        = string
    default     = "s-1vcpu-2gb"
}